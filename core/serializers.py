from rest_framework import serializers 
from core.models import Usuario

class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ('id',
            'nome',      
            'dataNacimento', 
            'fone',     
            'email',     
            'login',     
            'password',     
            'dataRegistro'  
            )






