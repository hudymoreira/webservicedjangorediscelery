from django.db import models

# Create your models here.
class Usuario(models.Model):
    nome          = models.CharField(max_length=200, null=True, default='')
    dataNacimento = models.DateField(null=True)
    fone          = models.CharField(max_length=200, null=True, default='')
    email         = models.CharField(max_length=200, null=True, default='')
    login         = models.CharField(max_length=200, null=True, default='')
    password      = models.CharField(max_length=200, null=True, default='')
    dataRegistro  = models.DateTimeField(auto_now=True,null=True)
    class Meta:
        ordering = ['nome']
        verbose_name = "usuario"
        verbose_name_plural = "usuarios"
    def __str__(self):
        return self.nome