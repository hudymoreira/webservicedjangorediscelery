from django.shortcuts import render

# Create your views here.
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status

from core.models import Usuario
from core.serializers import UsuarioSerializer
from rest_framework.decorators import api_view

@api_view(['GET'])
def listarUsuarios(request):
    if request.method == 'GET':
        usuarios = Usuario.objects.all()
        nome = request.GET.get('nome', None)
        if nome is not None:
            usuarios = usuarios.filter(nome__icontains=nome)
        users_serializer = UsuarioSerializer(usuarios,many=True)
        return JsonResponse(users_serializer.data, safe=False)
@api_view(['POST'])
def novoUsuario(request):
    if request.method == 'POST':
        usuario =JSONParser().parse(request)
        usuario_serializer = UsuarioSerializer(data=usuario)
        if usuario_serializer.is_valid():
            usuario_serializer.save()
            return JsonResponse(usuario_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return JsonResponse(usuario_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
@api_view(['GET', 'PUT', 'DELETE'])
def handleUsuario(request,id):
    try:
        usuario = Usuario.objects.get(pk=id)
        if request.method == 'GET':
            usuario_serializer = UsuarioSerializer(usuario)
            return JsonResponse(usuario_serializer.data)
        if request.method == 'PUT':
            usuario_data = JSONParser().parse(request)
            usuario_serializer = UsuarioSerializer(usuario, data=usuario_data)
            if usuario_serializer.is_valid():
                usuario_serializer.save()
                return JsonResponse(usuario_serializer.data)
            else:
                return JsonResponse(usuario_serializer.erros, status=status.HTTP_400_BAD_REQUEST)
        if request.method == 'DELETE':
            usuario.delete()
            return JsonResponse({'message': 'User deletado com sucesso!'}, status=status.HTTP_204_NO_CONTENT)
    except Usuario.DoesNotExist :
        return JsonResponse({'message': 'Usuario nao existe'}, status=status.HTTP_404_NOT_FOUND)
@api_view(['GET'])
def listarUsuarioPorNome(request,dados):
    usuarios = Usuario.objects.filter(nome__contains=dados)
    if request.method == 'GET':
        usuarios_serializer = UsuarioSerializer(usuarios, many=True)
        return JsonResponse(usuarios_serializer.data, safe=False)