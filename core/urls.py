from django.conf.urls import url 
from core import views

urlpatterns = [
    url(r'^api/usuarios$', views.listarUsuarios),
    url(r'^api/usuario/(?P<pk>[0-9]+)$', views.handleUsuario),
    url(r'^api/usuario/novo/', views.novoUsuario),
    url(r'^api/usuario/nome/(?P<dados>[-\w]+)/$', views.listarUsuarioPorNome)
]
